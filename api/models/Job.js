/**
 * Job.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  // connection: 'myMysqlServer',

  // migrate: 'alter',

  attributes: {
    
    //general area of the offered job. like Bussines, Computers, etc.
    lineofbis: { 
      type: 'string',
    },
    
    //general description of the job's role (as a title)
    role: { 
      type: 'string',
    },
    
     // up to 500 letters 
    description: { 
      type: 'string',
    },
    
    //this will be limited to : 'full', 'partial', 'shifts'
    scope: { 
      type: 'string',
    },
    
    //like student, ...
    type: { 
      type: 'string',
    },
    
    edited: {
      type: 'boolean'
    },

    deleted: {
      type: 'boolean'
    },

 
    // toJSON: function() { 
    //   var modelAttributes  = this.toObject();
    //   delete modelAttributes.password;
    //   delete modelAttributes.confirmation;
    //   delete modelAttributes.encryptedPassword;
    //   return modelAttributes;
    // }


  }
};

